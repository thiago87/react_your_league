import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {
  Drawer,
  Container,
  Header,
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,'
    + 'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,'
    + 'Shake or press menu button for dev menu',
});

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    height: 70,
    backgroundColor: '#c4c4c4',
    padding: 20,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#444444',
    marginBottom: 5,
  },
});

class SideBar extends React.PureComponent {
  render() {
    return (
      <Container />
    );
  }
}

export default class App extends React.Component {
  closeDrawer = () => {
    this.drawer._root.close(); // eslint-disable-line
  };

  openDrawer = () => {
    this.drawer._root.open(); // eslint-disable-line
  };

  render() {
    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<SideBar navigator={this.navigator} />}
        onClose={() => this.closeDrawer()}
      >
        <Header />
        <View style={[styles.header]}>
          <Icon onPress={() => this.openDrawer()} name="bars" size={30} color="#fff" />
        </View>
        <Container>
          <View style={styles.container}>
            <Text style={styles.welcome}>
              Welcome to React Native!
            </Text>
            <Text style={styles.instructions}>
              To get started, edit App.js
            </Text>
            <Text style={styles.instructions}>
              {instructions}
            </Text>
          </View>

        </Container>
      </Drawer>
    );
  }
}
