import React from 'react';
import {
  AsyncStorage,
  Button,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { ListItem, CheckBox } from 'react-native-elements';
import Modal from 'react-native-modal';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import { showMessage } from 'react-native-flash-message';
import { getGroup, addPayment, removePayment } from '../services/api';
import Colors from '../constants/Colors';
import Functions from '../constants/Functions';

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 12,
  },
  containerList: {
    height: 50,
    borderBottomColor: Colors.tintColor,
    borderBottomWidth: 2,
    borderTopWidth: 0,
  },
  dontPayment: {
    backgroundColor: '#ea9d9d',
  },
  alreadyPayment: {
    backgroundColor: '#FFF',
  },
});

function compare(a, b) {
  if (a.position < b.position) {
    return -1;
  }
  if (a.position > b.position) {
    return 1;
  }
  return 0;
}

export default class InternalGroupScreen extends React.Component {
  state = {
    group: {},
    teams: [],
    modalVisible: false,
    isAdmin: false,
    teamsPay: [],
    checkPaymentVisible: false,
    refreshing: false,
    spinner: false,
  };

  componentDidMount() {
    const { navigation } = this.props;
    this.sub = navigation.addListener(
      'willFocus',
      () => {
        this.getGroup();
      },
    );
  }

  componentWillUnmount() {
    this.sub.remove();
  }

  getGroup = async () => {
    const { navigation } = this.props;
    this.setState({ refreshing: true, spinner: true });
    Promise.all([
      AsyncStorage.getItem('user'),
      getGroup(navigation.getParam('id_group')),
    ])
      .then(([userStorage, group]) => {
        const user = JSON.parse(userStorage);
        if (group) {
          // verify if is admin
          if (group.users_groups) {
            const adminUsers = group.users_groups
              .find(obj => obj.is_admin === true && parseInt(obj.user_id, 10) === user.id);
            const teamsPay = group.users_groups
              .filter(obj => obj.has_paid)
              .map(obj => obj.user_id);
            const buttons = this.applyIcon(group, !!adminUsers);
            navigation.setParams(buttons);
            this.setState({ isAdmin: !!adminUsers, teamsPay });
          }
          this.setState({ group, teams: group.users_groups });
        }
      })
      .finally(() => this.setState({ refreshing: false, spinner: false }));
  }

  addOrRemovePayment = async (id, groupId, userId) => {
    const { teamsPay } = this.state;
    if (teamsPay.indexOf(userId) !== -1) {
      this.setState({ spinner: true });
      removePayment(id, { group_id: groupId, user_id: userId })
        .then(() => {
          showMessage({
            message: 'Pagamento removido',
            type: 'success',
          });
          teamsPay.splice(teamsPay.indexOf(userId), 1);
          this.setState({
            teamsPay,
          });
        })
        .catch(() => showMessage({
          message: 'Erro! Não foi possivel remover o pagamento.',
          type: 'danger',
        }))
        .finally(() => this.setState({ spinner: false }));
    } else {
      this.setState({ spinner: true });
      addPayment(id, { group_id: groupId, user_id: userId })
        .then(() => {
          showMessage({
            message: 'Pagamento adicionado',
            type: 'success',
          });
          teamsPay.push(userId);
          this.setState({ teamsPay });
        })
        .catch(() => showMessage({
          message: 'Erro! Não foi possivel adicionar pagamento.',
          type: 'danger',
        }))
        .finally(() => this.setState({ spinner: false }));
    }
  }

  togglePayment() {
    const { checkPaymentVisible } = this.state;
    this.setState({ checkPaymentVisible: !checkPaymentVisible });
  }

  applyIcon(data, isAdmin) {
    const { navigation } = this.props;
    const params = { id_group: navigation.getParam('id_group'), mode: 'edit' };
    const buttons = {
      icons: [{
        key: 0,
        onPress: () => this.infoGroup(),
        icon: 'info',
        show: true,
      }, {
        key: 1,
        onPress: () => this.togglePayment(),
        icon: 'dollar',
        show: isAdmin,
      }, {
        key: 2,
        onPress: () => this.openSearch(),
        icon: 'search',
        show: isAdmin,
      }, {
        key: 3,
        onPress: () => navigation.navigate('EditGroup', params),
        icon: 'pencil',
        title: data.name,
        show: isAdmin,
      }],
    };
    return buttons;
  }

  infoGroup() {
    this.setState({ modalVisible: true });
  }

  openSearch() {
    const { navigation } = this.props;
    const { group } = this.state;
    const hasTeams = group.users.map(t => t.time_id);
    navigation.navigate('PlayerSearchScreen', { id_group: navigation.getParam('id_group'), hasTeams });
  }

  openPlayer(team) {
    const { navigation } = this.props;
    navigation.navigate('PlayersScreen', { user_id: team });
  }

  render() {
    const {
      group, teams, modalVisible, isAdmin, teamsPay, checkPaymentVisible, refreshing, spinner,
    } = this.state;
    return (
      <View>
        <Spinner
          visible={spinner}
          overlayColor="rgba(206,206,206,0.75)"
        />
        { group
          && (
          <Modal isVisible={modalVisible}>
            <View style={{ flex: 1 }}>
              <View style={styles.content}>
                <Text style={styles.contentTitle}>{ group.name }</Text>
                <View>
                  {
                    group.group_type && (
                      <Text>
                        Período:
                        {group.group_type.name}
                        (
                          {group.started_session}
                        {' '}
                        à
                        {' '}
                        {group.finished_session}
                        )
                      </Text>
                    )
                  }
                  {
                    group.price && (
                    <Text>
                    R$:
                      { Functions.format_number(group.price) }
                    </Text>
                    )
                  }
                  {
                    group.payment_limit && (
                    <Text>
                    Pagamento:
                      { moment(group.payment_limit).format('DD/MM/YYYY') }
                    </Text>
                    )
                  }
                  {
                    group.group_awards && (
                      <Text style={styles.contentTitle}>Premiação</Text>
                    )
                  }
                  {
                    group.group_awards && group.group_awards.map((award, index) => (<Text key={index}>{index+1}º R$ {Functions.format_number(award.price)}</Text>)) // eslint-disable-line
                  }
                </View>
                <Button
                  onPress={() => this.setState({ modalVisible: false })}
                  title="Fechar"
                />
              </View>
            </View>
          </Modal>
          )
        }
        <ScrollView
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => this.getGroup(group.id)}
            />
          )}
        >
          {
            teams.sort(compare).map(team => (
              <ListItem
                key={team.user_id}
                // leftAvatar={{ source: { uri: team.foto.replace('FORMATO', '140x140')} }}4
                leftElement={checkPaymentVisible && (
                  <CheckBox
                    center
                    containerStyle={{ width: 10 }}
                    iconType="Click Here"
                    checkedIcon="clear"
                    uncheckedIcon="add"
                    uncheckedColor="#FFF"
                    checkedColor="red"
                    checked={(teamsPay.indexOf(team.user_id) !== -1)}
                    onPress={() => this.addOrRemovePayment(team.id, team.group_id, team.user_id)}
                  />
                )}
                title={team.team_name}
                subtitle={team.user_name}
                rightTitle={Functions.format_number(team.total_value)}
                rightSubtitle={`${team.position.toString()}º`}
                onPress={() => this.openPlayer(team.user_id)}
                containerStyle={[styles.containerList, isAdmin && teamsPay.indexOf(team.user_id) !== -1 ? styles.alreadyPayment : styles.dontPayment]}
              />
            ))
          }
        </ScrollView>
      </View>
    );
  }
}
