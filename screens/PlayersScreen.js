import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import { showMessage } from 'react-native-flash-message';
import ListPlayer from '../components/ListPlayer';
import { getPontuacao } from '../services/api';
import Functions from '../constants/Functions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  pagination: {
    flexDirection: 'row',
    backgroundColor: '#c2c2c2',
  },
  pagLeft: {
    flex: 1,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pagCenter: {
    flex: 8,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pagRight: {
    flex: 1,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spinnerTextStyle: {
    color: 'black',
  },
  list: {
    flex: 1,
  },
});

export default class PlayersScreen extends React.Component {
  state = {
    list: [],
    currentSession: null,
    screenSession: null,
    totalPoints: 0,
    spinner: false,
  };

  componentDidMount() {
    this.getPontuacao();
  }

  getPontuacao = (session = null) => {
    const { navigation } = this.props;
    const { currentSession } = this.state;
    const userId = navigation.getParam('user_id');
    this.setState({ spinner: true });
    getPontuacao(userId, session).then((data) => {
      if (data) {
        const points = data.reduce((sum, obj) => {
          if (obj.is_captain) {
            return sum + (obj.points * 2);
          }
          return sum + obj.points;
        }, 0);
        if (currentSession == null) {
          this.setState({ currentSession: data[0].session });
        }
        this.setState({
          list: data,
          screenSession: data[0].session,
          totalPoints: (Math.round(points * 100) / 100),
        });
      }
    }).catch((e) => {
      showMessage({
        message: e.message,
        type: 'danger',
      });
    }).finally(() => {
      this.setState({ spinner: false });
    });
  }

  prevRound = () => {
    const { screenSession } = this.state;
    this.getPontuacao(screenSession - 1);
  };

  nextRound = () => {
    const { screenSession } = this.state;
    this.getPontuacao(screenSession + 1);
  };

  render() {
    const {
      spinner, screenSession, currentSession, totalPoints, list,
    } = this.state;
    return (
      <View style={styles.container}>
        <Spinner
          visible={spinner}
          textStyle={styles.spinnerTextStyle}
          overlayColor="rgba(206,206,206,0.75)"
        />
        <View style={styles.pagination}>
          <View style={styles.pagLeft}>
            { screenSession != 1 && <Icon onPress={() => this.prevRound()} name="arrow-circle-o-left" size={24} style={{ color: '#FFF', padding: 10 }} /> }
          </View>
          <View style={styles.pagCenter}>
            <Text style={{ color: '#FFF' }}>
              Rodada
              {' '}
              {screenSession}
              {' '}
              -
              {' '}
            </Text>
            <Text style={{ fontWeight: 'bold', color: '#FFF', fontSize: 16 }}>
            (
              {Functions.format_number(totalPoints)}
            )
            </Text>
          </View>
          <View style={styles.pagRight}>
            { (currentSession) > screenSession && <Icon onPress={() => this.nextRound()} name="arrow-circle-o-right" size={24} style={{ color: '#fff', padding: 10 }} /> }
          </View>
        </View>
        <View style={styles.list}>
          <ListPlayer ref={r => this.refs = r} userPoints={list} />
        </View>
      </View>
    );
  }
}
