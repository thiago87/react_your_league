import React from 'react';
import {
  AsyncStorage, View, ScrollView, StyleSheet, RefreshControl,
} from 'react-native';
import { ListItem } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import { getGroups } from '../services/api';
import Colors from '../constants/Colors';

const styles = StyleSheet.create({
  containerList: {
    height: 50,
    borderBottomColor: Colors.tintColor,
    borderBottomWidth: 2,
    borderTopWidth: 0,
  },
  dontPayment: {
    backgroundColor: '#ea9d9d',
  },
});

export default class GroupScreen extends React.Component {
  state = {
    groups: [], refreshing: false, user: null, spinner: false,
  };

  componentDidMount() {
    const { navigation } = this.props;
    this.sub = navigation.addListener(
      'willFocus',
      () => {
        this.onLoad();
      },
    );
    const icons = {
      icons: [
        {
          key: 0,
          onPress: () => navigation.navigate('AddGroup', { mode: 'create' }),
          icon: 'plus',
          show: true,
        },
      ],
    };
    navigation.setParams(icons);
  }

  componentWillUnmount() {
    this.sub.remove();
  }

  onLoad = async () => {
    this.setState({ refreshing: true, spinner: true });
    Promise.all([
      AsyncStorage.getItem('user'),
      getGroups(),
    ])
      .then(([user, groups]) => {
        this.setState({ user: JSON.parse(user), groups });
      })
      .finally(() => {
        this.setState({ refreshing: false, spinner: false });
      });
  }

  getGroups = () => {
    this.setState({ refreshing: true });
    getGroups().then((data) => {
      if (data) {
        this.setState({ groups: data });
      }
    })
      .finally(() => {
        this.setState({ refreshing: false });
      });
  }

  openGroup = (id, name) => {
    const { navigation } = this.props;
    navigation.navigate('InternalGroup', { id_group: id, name_group: name });
  }

  render() {
    const {
 refreshing, groups, user, spinner,
} = this.state;
    return (
      <View>
        <Spinner
          visible={spinner}
          overlayColor="rgba(206,206,206,0.75)"
        />
        <ScrollView
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.getGroups}
            />
          )}
        >
          {
            groups && user && groups.map((group) => {
              const userGroup = group.users_groups.find(ug => parseInt(ug.user_id, 10) === user.id);
              return (
                <ListItem
                  key={group.id}
                  title={group.name}
                  subtitle={group.group_type.name}
                  rightTitle={`${userGroup.position.toString()}º`}
                  rightSubtitle={group.users.length.toString()}
                  onPress={() => this.openGroup(group.id, group.name)}
                  containerStyle={[styles.containerList, !userGroup.has_paid ? styles.dontPayment : null]}
                />
              );
            })
          }
        </ScrollView>
      </View>
    );
  }
}
