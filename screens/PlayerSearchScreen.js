import React from 'react';
import {
  Alert,
  View,
  ScrollView,
} from 'react-native';
import { ListItem } from 'react-native-elements';
import { Searchbar } from 'react-native-paper';
import { showMessage } from 'react-native-flash-message';
import Spinner from 'react-native-loading-spinner-overlay';
import { getTeams } from '../services/apiCartola';
import Colors from '../constants/Colors';
import { addUserGroup, removeUserGroup } from '../services/api';

export default class PlayerSearchScreen extends React.Component {
  state = {
    groupId: null, query: '', teams: [], teamChecked: [], loading: false, timeout: null, spinner: false,
  };

  componentDidMount() {
    const { navigation } = this.props;
    this.setState({ teamChecked: navigation.getParam('hasTeams'), groupId: navigation.getParam('id_group') });
  }

  componentWillUnmount() {
    const { timeout } = this.state;
    clearTimeout(timeout);
  }

  getTeams = async (query) => {
    this.setState({ spinner: true });
    getTeams(query)
      .then(result => this.setState({ teams: result }))
      .catch(e => showMessage({
        message: e.message,
        type: 'danger',
      }))
      .finally(() => this.setState({ loading: false, spinner: false }));
  }

  addPlayer = async (timeId) => {
    const { teamChecked, groupId } = this.state;

    this.setState({ spinner: true });
    addUserGroup({ group_id: groupId, time_id: timeId })
      .then(() => {
        showMessage({
          message: 'Adicionado com sucesso',
          type: 'success',
        });
        teamChecked.push(timeId);
      })
      .catch(() => {
        showMessage({
          message: 'Não foi possivel remover o time, tente novamente!',
          type: 'danger',
        });
      })
      .finally(() => this.setState({ spinner: false }));
  }

  removePlayer = async (timeId) => {
    const { teamChecked, groupId } = this.state;

    Alert.alert(
      'Confirmação',
      'Deseja realmente remover este time? Se o mesmo já estiver pago, não será possível recuperar a informação.',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Confirmar',
          onPress: () => {
            this.setState({ spinner: true });
            removeUserGroup({ group_id: groupId, time_id: timeId })
              .then(() => {
                showMessage({
                  message: 'Removido com sucesso',
                  type: 'success',
                });
                const tc = teamChecked.filter(e => e !== timeId);
                this.setState({ teamChecked: tc });
              })
              .catch(() => {
                showMessage({
                  message: 'Não foi possivel remover o time, tente novamente!',
                  type: 'danger',
                });
              })
              .finally(() => this.setState({ spinner: false }));
          },
        },
      ],
      { cancelable: false },
    );
  }

  addOrRemovePlayer(timeId) {
    const { teamChecked } = this.state;
    if (teamChecked.indexOf(timeId) !== -1) {
      this.removePlayer(timeId);
    } else {
      this.addPlayer(timeId);
    }
  }

  teamSearch(query) {
    const { timeout } = this.state;
    this.setState({ query, loading: true });

    clearTimeout(timeout);
    const tm = setTimeout(() => this.getTeams(query), 800);
    this.setState({ timeout: tm });
  }

  render() {
    const {
      teams, query, teamChecked, loading, spinner,
    } = this.state;

    return (
      <View>
        <Searchbar
          ref={(search) => {
            this.search = search;
          }}
          showLoading={loading}
          placeholder="Buscar time"
          onChangeText={text => this.teamSearch(text)}
          value={query}
        />
        <ScrollView>
          <Spinner
            visible={spinner}
            overlayColor="rgba(206,206,206,0.75)"
          />
          {
            teams.map(team => (
              <ListItem
                key={team.time_id}
                // leftAvatar={{ source: team.url_escudo_png && { uri: team.url_escudo_png } }}
                title={team.nome}
                subtitle={team.nome_cartola}
                checkBox={{
                  iconType: 'material',
                  checkedIcon: 'clear',
                  uncheckedIcon: 'add',
                  checkedColor: 'red',
                  checked: (teamChecked.indexOf(team.time_id) !== -1),
                  onPress: () => this.addOrRemovePlayer(team.time_id),
                }}
                containerStyle={{
                  borderBottomColor: Colors.tintColor,
                  borderBottomWidth: 2,
                  borderTopWidth: 0,
                }}
              />
            ))
          }
        </ScrollView>
      </View>
    );
  }
}
