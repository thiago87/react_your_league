import React, { Component } from 'react';
import {
  StatusBar,
  AsyncStorage,
  View,
  ActivityIndicator,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { showMessage } from 'react-native-flash-message';
import {
  Container,
  Input,
  Button,
  ButtonText,
  ErrorMessage,
} from '../components/styles';
import { login } from '../services/api';
import Color from '../constants/Colors';

export default class LoginScreen extends Component {
  state = {
    email: '',
    password: '',
    error: '',
    loading: false,
  };


  handleEmailChange = (email) => {
    this.setState({ email });
  };

  handlePasswordChange = (password) => {
    this.setState({ password });
  };

  handleSignInPress = async () => {
    const { email, password } = this.state;
    const { navigation } = this.props;
    if (email.length === 0 || password.length === 0) {
      this.setState({ error: 'Preencha usuário e senha para continuar!' }, () => false);
    } else {
      this.setState({ loading: true });
      const data = {
        email,
        password,
      };
      login(data).then((response) => {
        if (response.data) {
          AsyncStorage.setItem('user', JSON.stringify(response.data));
          const resetAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Home',
              }),
            ],
          });
          this.setState({ loading: false });
          navigation.dispatch(resetAction);
        } else {
          this.setState({ error: 'Não houve resposta do servidor, tente novamente' });
        }
      }).catch((error) => {
        showMessage({
          message: error.message,
          type: 'danger',
        });
        this.setState({ loading: false });
        this.setState({ error: 'Houve um problema com o login, verifique suas credenciais!' });
      });
    }
  };

  render() {
    const {
      email, password, error, loading,
    } = this.state;
    return (
      <Container>
        <StatusBar hidden />
        <Input
          placeholder="Endereço de e-mail"
          value={email}
          onChangeText={this.handleEmailChange}
          autoCapitalize="none"
          autoCorrect={false}
        />
        <Input
          placeholder="Senha"
          value={password}
          onChangeText={this.handlePasswordChange}
          autoCapitalize="none"
          autoCorrect={false}
          secureTextEntry
        />
        {error.length !== 0 && <ErrorMessage>{error}</ErrorMessage>}
        <Button onPress={this.handleSignInPress} disabled={loading}>
          <ButtonText>Entrar</ButtonText>
        </Button>
        {loading ? <View><ActivityIndicator size="large" color={Color.button} /></View> : null}
      </Container>
    );
  }
}
