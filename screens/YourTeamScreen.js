import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

export default class YourTeamScreen extends React.Component {
  static navigationOptions = {
    header: null,
    title: 'Sua liga',
  };

  render() {
    const { title } = this.props;
    return (
      <TouchableOpacity onPress={() => {}}>
        <View>
          <Text>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}
