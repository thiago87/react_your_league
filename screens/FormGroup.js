import React from 'react';
import {
  AsyncStorage,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  ScrollView,
  View,
  Text,
  Picker,
} from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux';
import {
  Field, FieldArray, reduxForm, formValueSelector, initialize,
} from 'redux-form';
import {
  TextInput,
  Switch,
  Paragraph,
  IconButton,
} from 'react-native-paper';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { TextInputMask } from 'react-native-masked-text';
import { showMessage } from 'react-native-flash-message';
import Spinner from 'react-native-loading-spinner-overlay';
import {
  Button,
  ButtonText,
} from '../components/styles';
import {
  getActiveGroupTypes, getGroup, addGroup, editGroup,
} from '../services/api';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
  },
  wrapper: {
    flex: 1,
  },
  inputContainerStyle: {
    margin: 8,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  awardInput: {
    flexDirection: 'column',
    flex: 1,
  },
  awardText: {
    width: 30,
    fontSize: 20,
    flexDirection: 'column',
  },
  awardView: {
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  awardButton: {
    flexDirection: 'row',
  },
  awardAdd: {
    flex: 1,
    alignItems: 'flex-start',
  },
  awardRemove: {
    flex: 1,
    alignItems: 'flex-end',
  },
});
class FormGroup extends React.Component {
  state = {
    pickerVisible: false,
    sending: false,
    groupTypes: [],
    mode: 'create',
    spinner: false,
  };

  componentDidMount() {
    const { navigation, change, dispatch } = this.props;
    this.setState({ spinner: true });
    Promise.all([
      AsyncStorage.getItem('user'),
      getActiveGroupTypes(),
      (navigation.getParam('mode') === 'edit' ? getGroup(navigation.getParam('id_group')) : {}),
    ])
      .then(([dataUser, groupTypes, group]) => {
        const user = JSON.parse(dataUser);
        change('user_id', user.id);
        this.setState({ groupTypes });
        if (Object.keys(group).length > 0) {
          this.setState({ mode: 'edit' });
          group.is_pay = group.price > 0;
          group.awards = [];
          group.group_type_id = parseInt(group.group_type_id, 10);
          group.group_awards.forEach(a => group.awards.push(a.price));
          dispatch(initialize(
            'formGroup',
            group,
          ));
        }
      })
      .catch(() => {
        showMessage({
          message: 'Ocorreu um erro',
          type: 'danger',
        });
      })
      .finally(() => this.setState({ spinner: false }));
  }

  renderAwardField = ({
    input, index, isPay, meta: { touched, error }, // eslint-disable-line
  }) => {
    const awardText = `${index + 1}º`;
    return (
      <View style={styles.awardView}>
        <Text style={styles.awardText}>{awardText}</Text>
        <Field
          disabled={!isPay}
          name={input.name}
          type="money"
          component={this.renderFieldMask}
          label={awardText}
          placeholder="Valor"
          style={styles.awardInput}
        />
        {/* {touched && error && <span>{error}</span>} */}
      </View>
    );
  }

  renderField = field => (
    <View>
      <TextInput
        {...field}
        mode="outlined"
        style={styles.inputContainerStyle}
        onChangeText={field.input.onChange}
        value={field.input.value}
      />
    </View>
  )

  renderFieldMask = field => (
    <TextInputMask
      disabled={field.disabled !== undefined ? field.disabled : false}
      type="money"
      customTextInput={TextInput}
      customTextInputProps={{
        mode: 'outlined',
        label: field.label,
        placeholder: field.placeholder,
      }}
      includeRawValueInChangeText={false}
      style={field.style ? field.style : styles.inputContainerStyle}
      onChangeText={field.input.onChange}
      value={field.input.value}
    />
  )

  renderFieldSwitch = field => (
    <View>
      <Switch
        onValueChange={field.input.onChange}
        value={field.input.value}
      />
    </View>
  )

  renderAwardFields = ({ fields, isPay, meta: { error, submitFailed } }) => ( // eslint-disable-line
    <View visible={isPay} style={styles.inputContainerStyle}>
      <View style={styles.awardButton}>
        <View style={styles.awardAdd}><IconButton icon="add-box" size={24} onPress={() => fields.push()} /></View>
        <View style={styles.awardRemove}><IconButton icon="cancel" size={24} onPress={() => fields.pop()} /></View>
      </View>
      {fields.map((price, index) => (
        <Field
          isPay={isPay}
          key={index} // eslint-disable-line
          name={price}
          index={index}
          component={this.renderAwardField}
        />
      ))}
    </View>
  )

  renderFieldSelect = field => (
    <TextInput
      label="Tipo"
      mode="outlined"
      value={field.input.value}
      style={styles.inputContainerStyle}
      render={() => (
        <Picker
          selectedValue={field.input.value}
          onValueChange={field.input.onChange}
        >
          <Picker.Item label="Selecione" value="0" />
          {field.items.map(item => <Picker.Item key={item.id} label={item.name} value={item.id} />)}
        </Picker>
      )}
    />
  )

  handleDatePicked = (date) => {
    const { change } = this.props;
    change('payment_limit', moment(date));
    this.setState({ pickerVisible: false });
  };

  handleSignInPress = (values) => {
    this.setState({ sending: true, spinner: true });
    const { navigation } = this.props;
    const { mode } = this.state;
    if (mode === 'create') {
      addGroup(values)
        .then((data) => {
          if (data) {
            showMessage({
              message: 'Grupo criado com sucesso',
              type: 'success',
            });
            navigation.goBack();
          } else {
            showMessage({
              message: 'Ocorreu um erro ao salvar o grupo, por favor tente novamente',
              type: 'danger',
            });
          }
        })
        .catch(e => showMessage({
          message: e.message,
          type: 'danger',
        }))
        .finally(() => this.setState({ sending: false, spinner: false }));
    } else if (mode === 'edit') {
      delete values.group_awards;
      delete values.group_type;
      delete values.users_groups;
      delete values.users;
      editGroup(values)
        .then((data) => {
          if (data) {
            showMessage({
              message: 'Grupo editado com sucesso',
              type: 'success',
            });
            navigation.goBack();
          } else {
            showMessage({
              message: 'Ocorreu um erro ao salvar o grupo, por favor tente novamente',
              type: 'danger',
            });
          }
        })
        .catch((e) => {
          showMessage({
            message: e.message,
            type: 'danger',
          });
        })
        .finally(() => this.setState({ sending: false }));
    }
  }

  render() {
    const { handleSubmit, isPay } = this.props;
    const {
      groupTypes, pickerVisible, sending, spinner,
    } = this.state;
    return (
      <KeyboardAvoidingView
        style={styles.wrapper}
        behavior="padding"
        keyboardVerticalOffset={80}
      >
        <Spinner
          visible={spinner}
          overlayColor="rgba(206,206,206,0.75)"
        />
        <ScrollView
          keyboardShouldPersistTaps="always"
          removeClippedSubviews={false}
        >
          <Field
            name="name"
            component={this.renderField}
            label="Nome"
            placeholder="Nome"
          />
          <Field
            name="group_type_id"
            component={this.renderFieldSelect}
            label="Tipo"
            items={groupTypes}
          />
          <View style={styles.row}>
            <Paragraph>Liga premiada</Paragraph>
            <View>
              <Field
                name="is_pay"
                component={this.renderFieldSwitch}
              />
            </View>
          </View>
          <Field
            disabled={!isPay}
            name="price"
            type="money"
            component={this.renderFieldMask}
            label="Valor"
            placeholder="Valor"
          />
          <TouchableWithoutFeedback onPress={() => {
            if (isPay) {
              this.setState({ pickerVisible: true });
            }
          }}
          >
            <View>
              <View pointerEvents="none">
                <Field
                  disabled={!isPay}
                  name="payment_limit"
                  component={this.renderField}
                  format={value => moment(value).format('DD/MM/YYYY')}
                  label="Limite de pagamento"
                  placeholder="Limite de pagamento"
                />
              </View>
            </View>
          </TouchableWithoutFeedback>
          <DateTimePicker
            isVisible={pickerVisible}
            onConfirm={(this.handleDatePicked)}
            onCancel={() => this.setState({ pickerVisible: false })}
          />
          <View style={styles.row}>
            <Paragraph>Liga publica</Paragraph>
            <View>
              <Field
                name="is_public"
                component={this.renderFieldSwitch}
              />
            </View>
          </View>
          <FieldArray name="awards" component={this.renderAwardFields} isPay={isPay} />
        </ScrollView>
        <Button onPress={handleSubmit(this.handleSignInPress)} disabled={sending}>
          <ButtonText>Salvar</ButtonText>
        </Button>
      </KeyboardAvoidingView>
    );
  }
}
const selector = formValueSelector('formGroup');
FormGroup = connect(
  (state) => {
    const isPay = selector(state, 'is_pay');

    return {
      isPay,
    };
  },
)(FormGroup);

export default reduxForm({
  form: 'formGroup',
  initialValues: {
    payment_limit: moment(new Date()),
    is_pay: true,
    is_public: false,
    user_id: 0,
  },
})(FormGroup);
