import React from 'react';
import {
  AsyncStorage,
  View,
  Text,
  SafeAreaView,
  Button,
} from 'react-native';
import {
  DrawerItems,
  createStackNavigator,
  createAppContainer,
  createDrawerNavigator,
  createMaterialTopTabNavigator,
  getActiveChildNavigationOptions,
} from 'react-navigation';
import Color from '../constants/Colors';
import TabBarIcon from '../components/TabBarIcon';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import LoginScreen from '../screens/LoginScreen';
import GroupScreen from '../screens/GroupScreen';
import FormGroup from '../screens/FormGroup';
import InternalGroupScreen from '../screens/InternalGroupScreen';
import PlayersScreen from '../screens/PlayersScreen';
import PlayerSearchScreen from '../screens/PlayerSearchScreen';
import DrawerContent from '../components/DrawerContent';

/**
 * - AppMainNavigator
 *    - LoginScreen
 *      - Form
 *    - DrawerNavigator
 *      - MainTopNavigator - DashboardStackNavigator
 *        - TabGroup
 *          - GroupScreen
 *        - InternalGroup
 *        - PlayersScreen
 *        - FormGroup
 */


// const DrawerContent = props => (
//   <View style={{
//     flex: 1,
//     flexDirection: 'column',
//     justifyContent: 'space-between',
//   }}
//   >
//     <View>
//       <View
//         style={{
//           backgroundColor: Color.tintColor,
//           height: 140,
//           alignItems: 'center',
//           justifyContent: 'center',
//         }}
//       >
//         <Text style={{ color: Color.errorText, fontSize: 30 }}>
//           Header
//         </Text>
//       </View>
//       <DrawerItems {...props} style={{ justifyContent: 'flex-start' }} />
//     </View>
//     <View>
//       <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
//         <Button
//           title="Sair"
//           onPress={() => {
//             AsyncStorage.clear();
//             props.navigation.navigate('Login');
//           }}
//           style={{
//             justifyContent: 'flex-end',
//           }}
//         />
//       </SafeAreaView>
//     </View>
//   </View>
// );

const navigationOptionsHeader = ({ navigation }) => ({
  headerStyle: {
    backgroundColor: Color.tintColor,
  },
  headerTintColor: Color.tintColor,
  headerTitleStyle: {
    fontWeight: 'bold',
    color: Color.errorText,
  },
  headerLeft: <HeaderLeft navigation={navigation} />,
});

const GroupNavigator = createStackNavigator({
  Home: {
    screen: GroupScreen,
    navigationOptions: ({ navigation }) => ({
      headerRight: <HeaderRight navigation={navigation} />,
    }),
  },
}, {
  defaultNavigationOptions: () => ({
    header: null,
  }),
});

const MainTopNavigator = createMaterialTopTabNavigator({
  TabGroup: {
    screen: GroupNavigator,
    navigationOptions: ({ navigation, screenProps }) => ({
      tabBarLabel: 'Grupos',
      ...getActiveChildNavigationOptions(navigation, screenProps),
    }),
  },
}, {
  initialRouteName: 'TabGroup',
  navigationOptions: ({ navigation, screenProps }) => {
    const childOptions = getActiveChildNavigationOptions(navigation, screenProps);
    return {
      headerRight: childOptions.headerRight,
    };
  },
});

const MainNavigator = createStackNavigator({
  MainNavigator: MainTopNavigator,
  AddGroup: {
    screen: FormGroup,
  },
  EditGroup: {
    screen: FormGroup,
  },
  InternalGroup: {
    screen: InternalGroupScreen,
    navigationOptions: ({ navigation }) => ({
      headerRight: <HeaderRight navigation={navigation} />,
      title: navigation.getParam('title'),
    }),
  },
  PlayersScreen,
  PlayerSearchScreen,
}, {
  initialRouteName: 'MainNavigator',
  defaultNavigationOptions: navigationOptionsHeader,

});

const DrawerNavigator = createDrawerNavigator({
  Home: {
    screen: MainNavigator,
    navigationOptions: {
      drawerLabel: 'Home',
      drawerIcon: () => <TabBarIcon name="ios-home" size={17} />,
    },
  },
  // Config: {
  //   screen: MainTopNavigator,
  //   navigationOptions: {
  //     drawerLabel: 'Configurações',
  //     drawerIcon: () => <TabBarIcon name="ios-settings" size={17} />,
  //   },
  // },
}, {
  contentComponent: DrawerContent,
  initialRouteName: 'Home',
});

const AppMainNavigator = createStackNavigator({
  Login: LoginScreen,
  Home: DrawerNavigator,
}, {
  initialRouteName: 'Login',
  defaultNavigationOptions: {
    header: null,
  },
});

const nav = createAppContainer(AppMainNavigator);

export default nav;
