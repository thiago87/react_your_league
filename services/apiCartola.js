import axios from 'axios';

const apiCartola = axios.create({
  baseURL: 'https://api.cartolafc.globo.com/',
  headers: {
    'Content-Type': 'application/json',
    'User-Agent': 'spider',
  },
});

export const getTeams = async teamName => apiCartola.get(`/times?q=${encodeURIComponent(teamName)}`)
  .then(response => response.data);

export default apiCartola;
