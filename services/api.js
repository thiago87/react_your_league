import axios from 'axios';
import { AsyncStorage } from 'react-native';

const api = axios.create({
  baseURL: 'http://apiyourleague.routeit.com.br/api/v1/',
  headers: {
    'Content-Type': 'application/json',
  },
});

api.interceptors.request.use(async (config) => {
  const token = await AsyncStorage.getItem('user');
  if (token) {
    config.headers.Authorization = JSON.parse(token).jwt; // eslint-disable-line
  }
  return config;
});

export const getMercadoStatus = async () => api.get('/?p=mercado/status').then(response => response.data);

export const getPontuacao = async (userId, rodada) => api.get(`/users/points_by_user/${userId}${(rodada ? `/${rodada}` : '')}`).then(response => response.data);

export const getMinhaPontuacao = async () => {
  const token = await AsyncStorage.getItem('@CartolaFC:token');
  return api.get('/', {
    p: `auth/time&token=${token}`,
  }).then(response => response.data);
};

export const login = async data => api.post('/users/login', data)
  .then(response => response.data);

/** Start routes GROUP */
export const getGroup = async groupId => api.get(`/groups/${groupId}`)
  .then(response => response.data);

export const getGroups = async (filters = null) => {
  let urlFilters = '';
  if (filters) {
    urlFilters = `?filters=${encodeURIComponent(JSON.stringify(filters))}`;
  }
  return api.get(`/groups${urlFilters}`)
    .then(response => response.data);
};

export const addGroup = async data => api.post('/groups', data)
  .then(response => response.data);

export const editGroup = async data => api.put(`/groups/${data.id}`, data)
  .then(response => response.data);

export const deleteGroup = async groupId => api.delete(`/groups/${groupId}`)
  .then(response => response.data);
/** Finish routes GROUP */

export const addUserGroup = async data => api.post('/users_groups', data)
  .then(response => response.data);

export const removeUserGroup = async data => api.delete(`/users_groups/remove_time/${data.group_id}/${data.time_id}`)
  .then(response => response.data);

export const addPayment = async (id, data) => api.put(`/users_groups/add_payment/${id}`, data)
  .then(response => response.data);

export const removePayment = async (id, data) => api.put(`/users_groups/remove_payment/${id}`, data)
  .then(response => response.data);

export const getActiveGroupTypes = async () => api.get('/group_types/active')
  .then(response => response.data);

export const getUser = async userId => api.get(`/users/${userId}`)
  .then(response => response.data);

export default api;
