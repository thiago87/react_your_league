module.exports = {
  'extends': 'airbnb',
  'parser': 'babel-eslint',
  'env': {
    'jest': true,
    "browser": true,
  },
  'rules': {
    'no-use-before-define': 2,
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    'comma-dangle': 2,
    'no-param-reassign': 'off',
    'mas-len': 'off',
  },
  'globals': {
    "fetch": false
  }
}