import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../constants/Colors';

const styles = StyleSheet.create({
  box: {
    flexDirection: 'row',
  },
});

export default class HeaderRight extends React.PureComponent {
  renderIcon = param => (
    <TouchableOpacity onPress={param.onPress} key={param.key}>
      <Icon
        name={param.icon}
        style={{
          color: Colors.errorText, padding: 10, marginRight: 10, fontSize: 20,
        }}
      />
    </TouchableOpacity>
  );

  render() {
    const { navigation } = this.props;
    const icons = navigation.getParam('icons');
    return (
      <View style={styles.box}>
        {
          icons && icons.map((param) => {
            if (param.show) {
              return this.renderIcon(param);
            }
          })
        }
      </View>
    );
  }
}
