import React from 'react';
import {
  View,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '../constants/Colors';

export default class HeaderLeft extends React.PureComponent {
  render() {
    const { navigation } = this.props;
    return (
      <View>
        <TouchableOpacity onPress={() => { navigation.openDrawer(); }}>
          <Icon
            name="bars"
            style={{
              color: Colors.errorText, padding: 10, marginLeft: 10, fontSize: 20,
            }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
