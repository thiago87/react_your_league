import React from 'react';
import { Icon } from 'expo';

import Colors from '../constants/Colors';

export default class TabBarIcon extends React.PureComponent {
  render() {
    const {
      onPress, name, size, focused,
    } = this.props;
    return (
      <Icon.Ionicons
        onPress={onPress}
        name={name}
        size={size || 23}
        style={{ marginBottom: -3 }}
        color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
      />
    );
  }
}
