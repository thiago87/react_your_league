import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { ListItem } from 'react-native-elements';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  badge: {
    backgroundColor: 'transparent',
  },
  positive_number: {
    color: '#00FF00',
  },
  negative_number: {
    color: '#FF0000',
  },
});

export default class ListTeam extends React.PureComponent {
  render() {
    const { teams } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView>
          {
  teams.map(team => (
    <ListItem
      key={team.id}
  // leftAvatar={{ source: { uri: team.foto.replace('FORMATO', '140x140')} }}
      title={team.name}
    />
  ))
  }
        </ScrollView>
      </View>
    );
  }
}

ListTeam.propTypes = {
  teams: PropTypes.array.isRequired, // eslint-disable-line
};
