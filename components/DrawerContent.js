import React from 'react';
import {
  AsyncStorage,
  Button,
  Image,
  SafeAreaView,
  Text,
  View,
} from 'react-native';
import { DrawerItems } from 'react-navigation';
import Colors from '../constants/Colors';

export default class DrawerContent extends React.PureComponent {
  state = { user: null };

  componentDidMount() {
    this.setUser();
  }

  setUser = async () => {
    const user = await AsyncStorage.getItem('user');
    this.setState({ user: JSON.parse(user) });
  }

  render() {
    const { navigation } = this.props;
    const { user } = this.state;

    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
      }}
      >
        <View>
          <View
            style={{
              backgroundColor: Colors.tintColor,
              height: 140,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Image
              style={{ width: 50, height: 50, borderRadius: 25 }}
              source={{ uri: user && user.foto_perfil }}
            />
            <Text style={{ color: Colors.errorText, fontSize: 13 }}>
              { user && user.nome_time }
            </Text>
          </View>
          <DrawerItems {...this.props} style={{ justifyContent: 'flex-start' }} />
        </View>
        <View>
          <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
            <Button
              title="Sair"
              onPress={() => {
                AsyncStorage.clear();
                navigation.navigate('Login');
              }}
              style={{
                justifyContent: 'flex-end',
              }}
            />
          </SafeAreaView>
        </View>
      </View>
    );
  }
}
