import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { ListItem } from 'react-native-elements';
import Colors from '../constants/Colors';
import Functions from '../constants/Functions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  badge: {
    backgroundColor: 'transparent',
  },
  positive_number: {
    color: Colors.positiveNumer,
  },
  negative_number: {
    color: Colors.negativeNumber,
  },
  listContainer: {
    height: 50,
    borderBottomColor: Colors.tintColor,
    borderBottomWidth: 2,
    borderTopWidth: 0,
  },
});

function compare(a, b) {
  if (a.player.position_id < b.player.position_id) {
    return -1;
  }
  if (a.player.position_id > b.player.position_id) {
    return 1;
  }
  return 0;
}

export default class ListPlayer extends React.PureComponent {
  render() {
    const { userPoints } = this.props;
    return (
      <View>
        <ScrollView>
          {
  userPoints.sort(compare).map((userPoint) => {
    const { player } = userPoint;
    return (
      <ListItem
        containerStyle={styles.listContainer}
        key={userPoint.id}
        leftAvatar={{ source: { uri: player.foto.replace('FORMATO', '140x140') } }}
        title={player.nickname}
        subtitle={player.position_name}
        badge={{
          value: Functions.format_number(userPoint.is_captain
            ? userPoint.points * 2
            : userPoint.points),
          textStyle: (userPoint.points > 0 ? styles.positive_number : styles.negative_number),
          badgeStyle: styles.badge,
        }}
      />
    );
  })
  }
        </ScrollView>
      </View>
    );
  }
}
