function queryBuild(data) {
  let uri = '';
  data.forEach((obj) => {
    uri += Object.keys(obj).map((k) => {
      if (k !== 'id') {
        return `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`;
      }
      return '';
    }).join('&');
  });
  return uri;
}

/* eslint-disable */
function format_number(number, decimals, decPoint, thousandsSep) {
  const dc = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
  const dp = typeof decPoint === 'undefined' ? ',' : decPoint;
  const ts = typeof thousandsSep === 'undefined' ? '.' : thousandsSep;
  const sign = number < 0 ? '-' : '';
  const i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(dc)));
  var j = (j = i.length) > 3 ? j % 3 : 0;

  return sign
        + (j ? i.substr(0, j) + ts : '')
        + i.substr(j).replace(/(\dp{3})(?=\dp)/g, `$1${ts}`)
        + (dc ? dp + Math.abs(number - i).toFixed(dc).slice(2) : '');
}
/* eslint-enable */

export default {
  queryBuild,
  format_number,
};
