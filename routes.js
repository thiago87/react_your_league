import { createStackNavigator } from 'react-navigation';

import LoginScreen from './screens/LoginScreen';
// import Main from './screens/Main';

const Routes = createStackNavigator({
  LoginScreen,
//   Main,
});

export default Routes;
